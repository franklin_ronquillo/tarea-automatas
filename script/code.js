function validacion() {
  const cadena = document.getElementById("cadena").value;
  const resultado = document.getElementById("resultado");

  const estadoInicial = 1;
  const estadoFinal = 3;
  var estadoActual = estadoInicial;

  var fin = false;

  let contador = 0;

  while (fin == false) {
    if (contador == cadena.length) {
      fin = true;
      break;
    }

    let simbolo = cadena.charCodeAt(contador);

    switch (estadoActual) {
      case 1:
        if (
          (simbolo >= 65 && simbolo <= 90) ||
          (simbolo >= 97 && simbolo <= 122)
        ) {
          estadoActual = 3;
        } else {
          estadoActual = 2;
        }
        contador++;
        break;

      case 2:
        contador++;
        break;

      case 3:
        if (
          (simbolo >= 65 && simbolo <= 90) ||
          (simbolo >= 97 && simbolo <= 122)
        ) {
          estadoActual = 3;
        } else if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 3;
        } else {
          estadoActual = 2;
        }
        contador++;
        break;
    }
  }

  if (estadoActual == estadoFinal) {
    resultado.textContent = "La cadena es válida.";
  } else {
    resultado.textContent = "La cadena no es válida.";
  }
}

function verificacion() {
  const cadena = document.getElementById("cadena1").value;
  const resultado = document.getElementById("resultado1");

  const estadoInicial = 1;
  let estadoFinal = [4, 7];
  let estadoActual = estadoInicial;

  var fin = false;

  let contador = 0;

  while (fin == false) {
    if (contador == cadena.length) {
      fin = true;
      break;
    }

    let simbolo = cadena.charCodeAt(contador);

    switch (estadoActual) {
      case 1:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 2;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;

      case 2:
        if (simbolo >= 48 && simbolo <= 57) {
          eActual = 2;
        } else if (simbolo == 46) {
          estadoActual = 3;
        } else if (simbolo == 69 || simbolo == 101) {
          estadoActual = 5;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case 3:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 4;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case 4:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 4;
        } else if (simbolo == 69 || simbolo == 101) {
          estadoActual = 5;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case 5:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 7;
        } else if (simbolo == 43 || simbolo == 45) {
          estadoActual = 6;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case 6:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 7;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case 7:
        if (simbolo >= 48 && simbolo <= 57) {
          estadoActual = 7;
        } else {
          estadoActual = Error;
        }
        contador++;
        break;
      case Error:
        contador++;
        break;
    }
  }
  if (estadoFinal.includes(estadoActual)) {
    resultado.textContent = "El número es válido.";
  } else {
    resultado.textContent = "El número no es válido.";
  }
}
